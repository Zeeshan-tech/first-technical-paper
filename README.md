
# CSS Flexbox and Grid

## Introduction
***

To simplify both the creation and maintenance of web pages, responsive layout models were created. Among the most popular are Flexbox and CSS Grid, each of which is widely supported across platforms and browsers. In this paper, we are going to look at both the powerful method and when we should use which models to create a beautiful and responsive layout

## Grid:
***

**CSS Grid Layout** is a two-dimensional grid-based layout system with rows and columns, making it easier to design web pages without having to use floats and positioning. Like tables, grid layout allows us to align elements into columns and rows.



![grid-image](https://blog.hubspot.com/hs-fs/hubfs/Heres%20the%20Difference%20Between%20Flexbox%2C%20CSS%20Grid%2C%20%26%20Bootstrap-1.png?width=466&name=Heres%20the%20Difference%20Between%20Flexbox%2C%20CSS%20Grid%2C%20%26%20Bootstrap-1.png)

## <ins>Grid Container</ins>

To make an HTML element behave as a grid container, you have to set the display property to grid or inline-grid.

Grid containers consist of grid items, placed inside columns and rows.

### 1. **The grid-template-columns Property**

T**he grid-template-columns** property defines the number of columns in your grid layout, and it can define the width of each column.

The value is a space-separated-list, where each value defines the width of the respective column.

If you want your grid layout to contain 4 columns, specify the width of the 4 columns, or "auto" if all columns should have the same width.

```
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto auto;
}
```
### 2. **The grid-template-rows Property**

**The grid-template-rows property** defines the height of each row.
The value is a space-separated-list, where each value defines the height of the respective row:


### 3. **The justify-content Property**

**The justify-content** property is used to align the whole grid inside the container.


### 4. **The align-content Property**

**The align-content** property is used to vertically align the whole grid inside the container.


## <ins>Child Elements (Items)</ins>

A grid container contains grid items.

By default, a container has one grid item for each column, in each row, but you can style the grid items so that they will span multiple columns and/or rows.

### 1. The grid-column Property:
The grid-column property defines on which column(s) to place an item.

You define where the item will start, and where the item will end.
```
.item1 {
  grid-column: 1 / 5;
}
```

### 2. The grid-row Property:
The grid-row property defines on which row to place an item.

You define where the item will start, and where the item will end.
```
.item1 {
  grid-row: 1 / 4;
}
```
### 3. The grid-area Property
The grid-area property can be used as a shorthand property for the grid-row-start, grid-column-start, grid-row-end and the grid-column-end properties.

```
.item1 {
  grid-area: 1 / 2 / 5 / 6;
}
```
## **Features of Grid**

- Clarity/Order — Grids bring order to a layout making it easier for visitors to find and navigate through information.
- Efficiency — Grids allow designers to quickly add elements to a layout because many layout decisions are addressed while building the grid structure.
- Economy — Grids make it easier for other designers to work and collaborate on the design as they provide a plan for where to place elements.
- Consistency/Harmony — Grids lead to consistency in the layout of pages across a single site or even several sites creating a structural harmony in the design.


# **CSS Flexbox**
The CSS Flexbox is a one-dimensional layout. It is useful in allocating and aligning the space among items in a grid container. It works with various kinds of display devices and screen sizes. Flex layout makes it easier to design and build responsive web pages without using many float and position properties in the CSS code.

![flexbox-image](https://blog.hubspot.com/hs-fs/hubfs/Heres%20the%20Difference%20Between%20Flexbox%2C%20CSS%20Grid%2C%20%26%20Bootstrap-3.png?width=519&name=Heres%20the%20Difference%20Between%20Flexbox%2C%20CSS%20Grid%2C%20%26%20Bootstrap-3.png)

To start using Flexbox, you have to create a flex container using the display: flex property. Every element inside the particular flex container will act as a flex item.

## Parent item (container)

```
<div class="flex-container">
     <div class="flex-item"></div>
    <div class="flex-item"></div>
    <div class="flex-item"></div>
</div>

<style>
.flex-container {
  display: flex;
}
</style>
```
### The flex container properties are:

- flex-direction 
      <p> The flex-direction property defines in which direction the container wants to stack the flex items.
- flex-wrap
      <p> The flex-wrap property specifies whether the flex items should wrap or not.
- flex-flow 
      <p> The flex-flow property is a shorthand property for setting both the flex-direction and flex-wrap properties.
- justify-content
      <p> The justify-content property is used to align the flex items:
- align-items
      <p> The align-items property is used to align the flex items.
- align-content
      <p> The align-content property is used to align the flex lines.



##  **Child Elements (Items)**
  <p> The direct child elements of a flex container automatically becomes flexible (flex) items.
 
## The flex item properties are:

- order
       <p> The order property specifies the order of the flex items.
       The order value must be a number, default value is 0.

- flex-grow 
       <p> The flex-grow property specifies how much a flex item will grow relative to the rest of the flex items.
       The value must be a number, default value is 0.
- flex-shrink
       <p> The flex-shrink property specifies how much a flex item will shrink relative to the rest of the flex items.
       The value must be a number, default value is 0.

- flex-basis
        <p>        The value must be a number, default value is 0.
- flex
        <p>  The flex property is a shorthand property for the flex-grow, flex-shrink, and flex-basis properties.


- align-self
     <p>  The align-self property specifies the alignment for the selected item inside the flexible container.


## Features of Flexbox
Following are the notable features of Flexbox layout −

1. Direction − You can arrange the items on a web page in any direction such as left to right, right to left, top to bottom, and bottom to top.

2. Order − Using Flexbox, you can rearrange the order of the contents of a web page.

3. Wrap − In case of inconsistent space for the contents of a web page (in single line), you can wrap them to multiple lines (both horizontally) and vertically.

4. Alignment − Using Flexbox, you can align the contents of the webpage with respect to their container.

5. Resize − Using Flexbox, you can increase or decrease the size of the items in the page to fit in available space.

## Usage of Grid and Flexbox
***
### You should consider using grid layout when: 

- You have a complex design to work with and want maintainable web pages
- You want to add gaps over the block elements
- You should consider using flexbox when:

- You have a small design to work with a few rows and columns
- You need to align the element
You don’t know how your content will look on the page, and you want everything to fit in.

### You should consider using Flexbox when:

- You have a small design to implement—it is ideal to use it when we have fewer rows and columns.
- You need to align elements—it is ideal for aligning elements in a one-dimensional direction.
- You need a content-first design—it is ideal when you want everything just to fit in.

# Conclusion

- CSS Grids helps you create the outer layout of the webpage. You can build complex as well responsive design with this. This is why it is called ‘layout first’.
- Flexbox mostly helps align content & move blocks.
- CSS grids are for 2D layouts. It works with both rows and columns.
- Flexbox works better in one dimension only (either rows OR columns).
  It will be more time saving and helpful if you use both at the same time.

# Refernces
1. [Mozila Documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
2. [tutorials Point](https://www.tutorialspoint.com/flexbox/index.htm)
3. [simplilearn](https://www.simplilearn.com/tutorials/css-tutorial/css-grid-vs-flexbox)
4. [scalar](https://www.scaler.com/topics/css/css-grid-vs-flexbox/)
5. [blog.hubspot.com/](https://blog.hubspot.com/website/css-grid-vs-flexbox)